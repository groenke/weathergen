Base.@kwdef struct WGENState{tType,prType,tempType}
    t::tType = Date(Dates.now(UTC))
    precip::prType = 0.0
    Tair::tempType = ComponentVector(min=0.0, max=0.0, mean=0.0)
end

function WGENState(state::WGENState; t=state.t, precip=state.precip, Tair=state.Tair)
    return WGENState(; t, precip, Tair)
end

include("wgen_precip.jl")

include("wgen_airtemp.jl")

Base.@kwdef struct WGENPara{precType,tempType,dtType}
    prec::precType = WGENPrecip()
    Tair::tempType = WGENAirTemp()
    dt::dtType = Day(1)
end

@gen function wgen_prior(para::WGENPara)
    prec = @trace(wgen_precip_prior(para.prec), :precip)
    Tair = @trace(wgen_airtemp_prior(para.Tair), :Tair)
    return ComponentVector(; prec, Tair)
end

"""
    wgen_kernel(i::Int, state::WGENState, para::WGENPara, p)

Generative kernel for a WGEN model; takes the time step index, current state,
parameterization, and parameter values and returns the state for the next time step.
"""
@gen (static) function wgen_kernel(i::Int, state::WGENState, para::WGENPara, p)
    t = state.t + para.dt
    newstate = WGENState(state; t)
    precip = @trace(wgen_precip(i, newstate, para.prec, p.prec), :precip)
    newstate = WGENState(newstate; precip)
    Tair = @trace(wgen_air_temperature(i, newstate, state, para.Tair, p.Tair), :Tair)
    newstate = WGENState(newstate; Tair)
    return newstate
end

@gen (static) function wgen_chain(params, initialstate, para::WGENPara, nsteps::Int)
    outputs = @trace(Unfold(wgen_kernel)(nsteps, initialstate, para, params), :state)
    return outputs
end

@gen (static) function wgen_joint_model(para::WGENPara, initialstate, prior_args::Tuple, nsteps::Int)
    params = @trace(wgen_prior(prior_args[1]), :para)
    outputs = @trace(wgen_chain(params, initialstate, para, nsteps), :model)
    return outputs
end

function get_timestamps_from(trace)
    retval = Gen.get_retval(trace)
    return collect(map(state -> state.t, retval))
end

function get_para_from(trace)
    choices = Gen.get_choices(trace)
    return Gen.get_selected(choices, Gen.select(:para))
end
