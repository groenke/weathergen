"""
Parameter struct for WGEN precipitation generator.
"""
Base.@kwdef struct WGENPrecip{pddType,pwdType,shapeType,meanType}
    pdd::pddType = FourierParam(constraint=UnitInterval())
    pwd::pwdType = FourierParam(constraint=UnitInterval())
    gamma_shape::shapeType = PositiveParam(median=1.0, transformed_scale=1.0)
    gamma_mean::meanType = FourierParam(constraint=Positive(), level_median=5.0, level_scale=1.0)
end


"""
    @gen wgen_precip(i::Int, state::WGENState, para::WGENPrecip, p)

Transition kernel for the WGEN precipitation model.
"""
@gen function wgen_precip(i::Int, state::WGENState, para::WGENPrecip, p)
    pdd = apply_para(para.pdd, p.pdd, state)
    pwd = apply_para(para.pdd, p.pdd, state)
    gamma_shape = apply_para(para.gamma_shape, p.gamma_shape, state)
    gamma_mean = apply_para(para.gamma_mean, p.gamma_mean, state)
    gamma_scale = gamma_mean / gamma_shape
    p = ifelse(state.precip > 0, pwd, pdd)
    dry ~ bernoulli(p)
    precip = if dry
        zero(state.precip)
    else
        preicp ~ gamma(gamma_shape, gamma_scale)
    end
    return precip
end

"""
    @gen (static) wgen_precip_prior(para::WGENPrecip)

Defines a generative function from the given WGEN parameterization `para`
that returns samples from the prior.
"""
@gen function wgen_precip_prior(para::WGENPrecip)
    pdd_prior = get_para_prior(para.pdd)
    pwd_prior = get_para_prior(para.pwd)
    gamma_shape_prior = get_para_prior(para.gamma_shape)
    gamma_mean_prior = get_para_prior(para.gamma_mean)
    pdd = @trace(pdd_prior(para.pdd), :pdd)
    pwd = @trace(pwd_prior(para.pwd), :pwd)
    gamma_shape = @trace(gamma_shape_prior(para.gamma_shape), :gamma_shape)
    gamma_mean = @trace(gamma_mean_prior(para.gamma_mean), :gamma_mean)
    return ComponentVector(; pdd, pwd, gamma_shape, gamma_mean)
end

function get_precip_from(trace)
    retval = Gen.get_retval(trace)
    return collect(map(state -> state.precip, retval))
end

function wgen_precip_obs(idx::Int, precip)
    if ismissing(precip)
        choicemap()
    elseif precip > 0
        choicemap((:model => :state => idx => :dry, false), (:model => :state => idx => :precip, precip))
    else
        choicemap((:model => :state => idx => :dry, true),)
    end
end

"""
    DryWetParam{dryType,wetType}

Allows for separate parameterizations for both dry and wet states.
"""
Base.@kwdef struct DryWetParam{dryType,wetType}
    dry::dryType
    wet::wetType
end

@gen function dry_wet_para_prior(para::DryWetParam)
    dry_prior = get_para_prior(para.dry)
    wet_prior = get_para_prior(para.wet)
    dry = @trace(dry_prior(para.dry), :dry)
    wet = @trace(wet_prior(para.wet), :wet)
    return ComponentVector(; dry, wet)
end

function apply_para(para::DryWetParam, p, state)
    if state.precip > 0
        return apply_para(para.wet, p.wet, state)
    else
        return apply_para(para.dry, p.dry, state)
    end
end

get_para_prior(::DryWetParam) = dry_wet_para_prior
