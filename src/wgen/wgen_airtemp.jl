"""
Parameter struct for WGEN air temperature generator.
"""
Base.@kwdef struct WGENAirTemp{minType,maxType,meanType,acType,residType,scaleType}
    mean::meanType = MonthlyParam()
    min::minType = MonthlyParam()
    max::maxType = MonthlyParam()
    autocor::acType = UnitParam(median=0.5, transformed_scale=0.2)
    mean_resid_scale::residType = PositiveParam(median=4.0, transformed_scale=0.5)
    offset_scale::scaleType = PositiveParam(median=3.0, transformed_scale=0.5)
end

@gen function wgen_airtemp_prior(para::WGENAirTemp)
    # mean, min, max parameters
    mean_prior = get_para_prior(para.mean)
    min_prior = get_para_prior(para.min)
    max_prior = get_para_prior(para.max)
    mean = @trace(mean_prior(para.mean), :mean)
    min = @trace(min_prior(para.min), :min)
    max = @trace(max_prior(para.max), :max)
    # process parameters
    rho_prior = get_para_prior(para.autocor)
    Tmean_resid_scale_prior = get_para_prior(para.mean_resid_scale)
    offset_scale_prior = get_para_prior(para.offset_scale)
    rho = @trace(rho_prior(para.autocor), :rho)
    Tmean_resid_scale = @trace(Tmean_resid_scale_prior(para.mean_resid_scale), :Tmean_resid_scale)
    Tmin_offset_scale = @trace(offset_scale_prior(para.offset_scale), :Tmin_offset_scale)
    Tmax_offset_scale = @trace(offset_scale_prior(para.offset_scale), :Tmax_offset_scale)
    return ComponentVector(; mean, min, max, rho, Tmean_resid_scale, Tmin_offset_scale, Tmax_offset_scale)
end

@gen function wgen_air_temperature(i::Int, state::WGENState, prevstate::WGENState, para::WGENAirTemp, p)
    # mean
    Tmean_prev_mean = apply_para(para.mean, p.mean, prevstate)
    Tmean_prev = prevstate.Tair.mean
    Tmean_prev_resid = Tmean_prev - Tmean_prev_mean
    
    # get new seasonal mean values from current state
    Tmean_mean = apply_para(para.mean, p.mean, state)
    Tmin_mean = apply_para(para.min, p.min, state)
    Tmax_mean = apply_para(para.max, p.max, state)

    # generate new daily mean
    Tmean_resid ~ normal(0.0, 1.0)
    mean = p.rho[1]*Tmean_prev_resid + Tmean_resid*p.Tmean_resid_scale[1] + Tmean_mean

    # generate min/max
    min = mean - p.Tmin_offset_scale[1]*(Tmean_mean - Tmin_mean)
    max = mean + p.Tmax_offset_scale[1]*(Tmax_mean - Tmean_mean)
    return ComponentVector(; mean, min, max)
end

function get_airtemp_from(trace)
    retval = Gen.get_retval(trace)
    return collect(map(state -> state.Tair, retval))
end
