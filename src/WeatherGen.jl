module WeatherGen

using ComponentArrays
using Dates
using Setfield

# statistics, signal processing, and math
using FFTW
using LinearAlgebra
using Statistics, StatsFuns

# probabilistic modeling
using Gen
using SimulationBasedInference

export precip_summary_stats, tair_summary_stats, cumulative_dry_days
include("utils.jl")

include("gen_distributions.jl")

export LocScaleParam, UnitParam, PositiveParam, FourierParam
export parameterize_monthly, parameterize_monthly_hierarchical
include("reparam/reparam.jl")

export WGENState, WGENPara, WGENPrecip, WGENAirTemp, DryWetParam
export wgen_prior, wgen_chain, wgen_joint_model
include("wgen/wgen.jl")

end # module WeatherGen
