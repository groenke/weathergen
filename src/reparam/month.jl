Base.@kwdef struct MonthlyParam{valType,tType}
    values::NTuple{12,valType} = Tuple(zeros(12))
    t0::tType = Date(0)
end

get_para_prior(para::MonthlyParam) = monthly_para_prior

@gen function monthly_para_prior(para::MonthlyParam)
    # retrieve prior definitions for each month
    priors = [get_para_prior(para.values[i]) for i in 1:12]
    # trace priors
    months = [Symbol(i) => @trace(priors[i](para.values[i]), Symbol(i)) for i in 1:12]
    return ComponentVector(; months...)
end

function apply_para(para::MonthlyParam, p, state)
    # get current and next month
    t = state.t
    m = month(t)
    # get endpoints (start of each month)
    t⁻ = Date(year(t), m)
    if m == 12
        m⁺ = 1
        t⁺ = Date(year(t)+1, m⁺)
    else
        m⁺ = m+1
        t⁺ = Date(year(t), m⁺)
    end
    # apply parameterizations recursively to current and next month knots
    y⁻ = apply_para(para.values[m], getproperty(p, Symbol(m)), @set(state.t = t⁻)) 
    y⁺ = apply_para(para.values[m⁺], getproperty(p, Symbol(m⁺)), @set(state.t = t⁺))
    # convert to real-valued time in days
    τ⁻ = Dates.value(convert(Day, t⁻ - para.t0))
    τ⁺ = Dates.value(convert(Day, t⁺ - para.t0))
    @assert τ⁺ > τ⁻
    # linearly interpolate between knots
    τ = Dates.value(convert(Day, t - para.t0))
    return y⁻ + (τ - τ⁻)*(y⁺ - y⁻)/(τ⁺ - τ⁻)
end

function parameterize_monthly(
    timestamps::AbstractVector{<:TimeType},
    values::AbstractVector{<:Real};
    agg=mean,
    t0=Date(0),
)
    # get months
    months = month.(timestamps)
    values = map(1:12) do m
        idx = findall(==(m), months)
        agg(values[idx])
    end
    return MonthlyParam(; values=Tuple(values), t0)
end

function parameterize_monthly_hierarchical(
    ::Type{LocScaleParam},
    timestamps::AbstractVector{<:TimeType},
    values::AbstractVector{<:Real};
    t0=Date(0),
)
    # get months
    months = month.(timestamps)
    values = map(1:12) do m
        idx = findall(==(m), months)
        μ = mean(values[idx])
        σ = std(values[idx])
        LocScaleParam(μ, σ)
    end
    return MonthlyParam(; values=Tuple(values), t0)
end

function parameterize_monthly_hierarchical(
    ::Type{PositiveParam},
    timestamps::AbstractVector{<:TimeType},
    values::AbstractVector{<:Real};
    t0=Date(0),
)
    # get months
    months = month.(timestamps)
    values = map(1:12) do m
        idx = findall(==(m), months)
        m = mean(values[idx])
        s = std(values[idx])
        d = SBI.from_moments(LogNormal, m, s)
        PositiveParam(median=median(d), transformed_scale=d.σ)
    end
    return MonthlyParam(; values=Tuple(values), t0)
end

function parameterize_monthly_hierarchical(
    ::Type{UnitParam},
    timestamps::AbstractVector{<:TimeType},
    values::AbstractVector{<:Real};
    t0=Date(0),
)
    # get months
    months = month.(timestamps)
    values = map(1:12) do m
        idx = findall(==(m), months)
        q25 = quantile(values[idx], 0.25)
        q75 = quantile(values[idx], 0.75)
        d = SBI.from_quantiles(LogitNormal(0,1), 0.25 => q25, 0.75 => q75)
        UnitParam(median=median(d), transformed_scale=d.σ)
    end
    return MonthlyParam(; values=Tuple(values), t0)
end
