const DEFAULT_FREQS = [1/365]

Base.@kwdef struct FourierParam{conType,valType,tType}
    constraint::conType = Unconstrained()
    freqs::Vector{valType} = DEFAULT_FREQS
    level_median::valType = default_median(constraint)
    level_scale::valType = level_median/2
    amp_median::valType = default_amplitude(constraint)
    amp_scale::valType = amp_median/2
    t0::tType = Date(0)
end

default_median(::Unconstrained) = 0.0
default_median(::Positive) = 1.0
default_median(::UnitInterval) = 0.5

default_amplitude(::Unconstrained) = 1.0
default_amplitude(::Positive) = 1.0
default_amplitude(::UnitInterval) = 0.5

"""
    apply_para(para::FourierParam, p::ComponentVector, state)

Evaluates the Fourier series parameterization `para` given parameters `p` and time `t`.
"""
function apply_para(para::FourierParam, p::ComponentVector, state)
    ωs = para.freqs
    normalized_weights = softmax(p.weights)
    τ = Dates.value(convert(Day, state.t - para.t0))
    offset = sum(enumerate(ωs)) do (i, ω)
        # note that we apply the phase shift as a linear combination of
        # sine and cosine to ensure that the amplitude is preserved
        ϕ = p.phase_shift[i]
        normalized_weights[i]*(cos(ϕ)*sin(2π*ω*τ) + sin(ϕ)*cos(2π*ω*τ))
    end
    return p.level + p.amp*offset
end

@gen function unconstrained_fourier_para_prior(para::FourierParam{Unconstrained})
    level ~ loc_scale_normal(para.level_median, para.level_scale)
    amp ~ lognormal(log(para.amp_median), para.amp_scale)
    # sample weights and phase shift
    N = length(para.freqs)
    weights ~ loc_scale_diag_normal(zeros(N), ones(N))
    phase_shift ~ broadcast_uniform(-π/2, π/2, N)
    return ComponentVector(; level, amp, weights, phase_shift)
end

@gen function positive_fourier_para_prior(para::FourierParam{Positive})
    log_level ~ loc_scale_normal(log(para.level_median), para.level_scale)
    # derive amplitude scaling factor median as fraction of level_median, clamping to prevent invalid values
    f_amp_median = clamp(para.amp_median / para.level_median, eps(), 1-eps())
    f_amp ~ logitnormal(logit(f_amp_median), para.amp_scale)
    level = exp(log_level)
    amp = f_amp*abs(level)
    # sample weights and phase shift
    N = length(para.freqs)
    weights ~ loc_scale_diag_normal(zeros(N), ones(N))
    phase_shift ~ broadcast_uniform(-π/2, π/2, N)
    return ComponentVector(; level, amp, weights, phase_shift)
end

@gen function unit_fourier_para_prior(para::FourierParam{UnitInterval})
    level ~ logitnormal(logit(para.level_median), para.level_scale)
    # derive amp_scale median as fraction of level_median, clamping to prevent invalid values
    f_amp_median = clamp(para.amp_median / para.level_median, eps(), 1-eps())
    f_amp ~ logitnormal(logit(f_amp_median), para.amp_scale)
    amp = min(f_amp*abs(level), f_amp*(1 - abs(level)))
    # sample weights and phase shift
    N = length(para.freqs)
    weights ~ loc_scale_diag_normal(zeros(N), ones(N))
    phase_shift ~ broadcast_uniform(-π/2, π/2, length(para.freqs))
    return ComponentVector(; level, amp, weights, phase_shift)
end

get_para_prior(::FourierParam{Unconstrained}) = unconstrained_fourier_para_prior
get_para_prior(::FourierParam{Positive}) = positive_fourier_para_prior
get_para_prior(::FourierParam{UnitInterval}) = unit_fourier_para_prior

function FourierParam(
    timestamps::AbstractVector{<:TimeType},
    values::AbstractVector{<:Real},
    constraint::Constraint=Unconstrained();
    t0=Date(0),
)
    # get link function for constraint
    f = link(constraint)
    error("not yet implemented")
end