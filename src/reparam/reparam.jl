# reparameterization constraint types
abstract type Constraint end
struct Unconstrained <: Constraint end
struct Positive <: Constraint end
struct UnitInterval <: Constraint end

link(::Constraint) = identity
link(::Positive) = log
link(::UnitInterval) = logit

@gen (static) function default_identity_prior(x)
    return x
end

"""
    get_para_prior(x)

Retrieves the generative function for the prior of parameterization `x`.
Dispatches of this method should be defined for each parameterization type.
"""
get_para_prior(x) = default_identity_prior

"""
    apply_para(::paraType, p, state)

Applies the reparameterization transform for the given parameterization with parameters `p`
and `state`. Dispatches of this method should be defined for each parameterization type.
"""
apply_para(::paraType, p, state) where {paraType} = p

Base.@kwdef struct LocScaleParam{T}
    loc::T = 0.0
    scale::T = 1.0
end

@gen function loc_scale_prior(para::LocScaleParam)
    scale = para.scale
    ϵ ~ normal(0, 1)
    loc ~ normal(para.loc, scale)
    return ComponentVector(; ϵ, loc, scale)
end

function apply_para(::LocScaleParam, p, state)
    return p.ϵ*p.scale + p.loc
end

get_para_prior(::LocScaleParam) = loc_scale_prior

Base.@kwdef struct ConstrainedParam{conType,T}
    constraint::conType = Positive()
    median::T = isa(constraint, Positive) ? 1.0 : 0.5
    transformed_scale::T = 1.0
end

const PositiveParam{T} = ConstrainedParam{Positive,T} where {T}
const UnitParam{T} = ConstrainedParam{UnitInterval,T} where {T}

PositiveParam(; kwargs...) = ConstrainedParam(; constraint=Positive(), kwargs...)
UnitParam(; kwargs...) = ConstrainedParam(; constraint=UnitInterval(), kwargs...)

@gen function unit_para_prior(para::UnitParam)
    logit_z ~ normal(logit(para.median), para.transformed_scale)
    return ComponentVector(; logit_z)
end

@gen function positive_para_prior(para::PositiveParam)
    log_z ~ normal(log(para.median), para.transformed_scale)
    return ComponentVector(; log_z)
end

function apply_para(::PositiveParam, p, state)
    return exp(p.log_z)
end

function apply_para(::UnitParam, p, state)
    return logistic(p.logit_z)
end

get_para_prior(::PositiveParam) = positive_para_prior
get_para_prior(::UnitParam) = unit_para_prior

param(::Unconstrained, μ, σ) = LocScaleParam(μ, σ)
param(::Positive, μ, σ) = PositiveParam(median=exp(μ), transformed_scale=σ)
param(::UnitInterval, μ, σ) = UnitParam(median=logistic(μ), transformed_scale=σ)

include("fourier.jl")

include("month.jl")
