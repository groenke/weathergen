"""
    cumulative_dry_days(prec::AbstractVector{T}, agg=mean) where {T}

Computes cumulative dry days (CDD) over precipication series `prec` with aggregation function `agg`.
"""
function cumulative_dry_days(prec::AbstractVector{T}, agg=mean) where {T}
    dry_spells = [0]
    for i in eachindex(prec)
        if prec[i] > zero(T) && dry_spells[end] > 0
            push!(dry_spells, 0)
        elseif iszero(prec[i])
            dry_spells[end] += 1
        end
    end
    return agg(dry_spells)
end

"""
    precip_summary_stats(ts::AbstractVector, prec::AbstractVector)

Default set of precipication summary statistics: 99th percentile, mean precip,
total preicp, wet days, and cumulative dry days.
"""
function precip_summary_stats(ts::AbstractVector, prec::AbstractVector)
    nzprec = filter(>(0), prec)
    p99 = length(nzprec) > 0 ? quantile(nzprec, 0.99) : 0.0
    # p01 = length(nzprec) > 0 ? quantile(filter(>(0), prec), 0.01) : 0.0
    pmean = mean(nzprec)
    ptot = sum(prec)
    wd = sum(prec .> 0.0)
    cdd = cumulative_dry_days(prec)
    # prec_tarr = TimeArray(ts, prec)
    # ptot_monthly = collapse(prec_tarr, month, first, sum)
    stats = ComponentVector(; p99, pmean, ptot, wd, cdd)
    return stats
    # return vcat(stats, values(ptot_monthly))
end

"""
    tair_summary_stats(ts::AbstractVector, Tair::AbstractVector)

Default set of air temperature summary statistics.
"""
function tair_summary_stats(ts::AbstractVector, Tair::AbstractVector)
    Tair_mean = mean(Tair)
    Tmax = maximum(Tair)
    Tmin = minimum(Tair)
    DT_mean = mean(diff(Tair))
    Tair_ac = cor(Tair[2:end] .- Tair_mean, Tair[1:end-1] .- Tair_mean)
    return ComponentVector(; Tair_mean, Tmax, Tmin, DT_mean, Tair_ac)
end

"""
    fourier_feats(t, freqs=[1/365.25]; intercept=true)

Return a set of Fourier features (i.e. basis functions) evaluated over time stamps `t`
with frequencies `freqs`. The number of basis functions will be equal to `2*length(freqs)`;
if `intercept=true`, an additional column of ones will be included to represent a constant term.
"""
function fourier_feats(t, freqs=[1/365.25]; intercept=true)
    t = reshape(t, (:,1))
    freqs = reshape(freqs, (1,:))
    shift = ones((size(t,1),1))
    s = sin.(2*π*t*freqs)
    c = cos.(2*π*t*freqs)
    xs = intercept ? [shift, s, c] : [s,c]
    X = reduce(hcat, xs)
    return X
end

"""
    fourier_lsq(t, y, freqs=[1/365.25]; intercept=true)

Generates fourier features for the given time coorindate `t` and computes least squares
regression on `y`, returning the coefficients as well as the design matrix `X`.
"""
function fourier_lsq(t, y, freqs=[1/365.25]; intercept=true)
    @assert size(t,1) == size(y,1) "number of timesteps must match number of observations"
    t = reshape(t, (:,1))
    X = fourier_feats(t, freqs; intercept)
    coef = pinv(X)*y
    return coef, X
end