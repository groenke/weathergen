StatsFuns.logistic(b::Gen.DistWithArgs{T}) where T <: Real =
    Gen.DistWithArgs(Gen.TransformedDistribution{T, T}(b.base, 0, logistic, logit, x -> (1.0 / (x-x^2),)), b.arglist)

@dist loc_scale_normal(μ, σ) = μ + normal(0, 1)*σ

@gen function loc_scale_diag_normal(μ, σ, f=identity)
    ϵ ~ mvnormal(zeros(length(μ)), Diagonal(ones(length(σ))))
    return f.(μ .+ ϵ.*σ)
end

@gen function broadcast_uniform(lo, hi, n::Int)
    return [@trace(uniform(lo, hi), Symbol(i)) for i in 1:n]
end

@gen log_diag_normal(μ, σ) = loc_scale_diag_normal(μ, σ, exp)

@gen logit_diag_normal(μ, σ) = loc_scale_diag_normal(μ, σ, logistic)

@gen (static) function lognormal(μ, σ)
    log_ϵ ~ loc_scale_normal(μ, σ)
    return exp(log_ϵ)
end

@gen (static) function logitnormal(μ, σ)
    logit_ϵ ~ loc_scale_normal(μ, σ)
    return logistic(logit_ϵ)
end
