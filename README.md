# WeatherGen

Flexible weather generation tools in Python and Julia.

## Quick start (python)

The WGEN-type weather generators can be easily set up

```python
# define name map for columns (optional)
name_map = wx.data.data_var_name_map(prec="pre", Tair_mean="tavg", Tair_min="tmin", Tair_max="tmax")
# load dataset
basin_data = wx.data.load_time_series_csv("path/to/data_file.csv", name_map)
# construct WGEN
wgen = wx.WGEN(basin_data_train)
# fit using stochastic variational inference
fit_result = wgen.fit("svi")
```

Note that the further development of the python code for this project will continue on [GitHub](https://github.com/bgroenks96/wxsbi).
