using Distributed

const project_dir = dirname(Base.active_project())

# start worker processes
workers = addprocs(64, exeflags=["--project=$(project_dir)"])

@everywhere begin

using CSV, DataFrames, HDF5, Rasters, TimeSeries
using Dates
using Impute
using Statistics

using Turing

# plotting
import CairoMakie as Makie

include("lto_data_util.jl")
include("forward_models.jl")
include("inference.jl")

end

freezing_degree_days(T::DimArray, Tair::DimArray) = sum(ustrip.(T[Tair .<= 0.0]), dims=Ti)[1]
thawing_degree_days(T::DimArray, Tair::DimArray) = sum(ustrip.(T[Tair .> 0.0]), dims=Ti)[1]

@everywhere begin

function make_forward_prob(Tair, Ts, soilprofile, tspan; nf=0.6, nt=0.9)
    return soil_heat_model_Tair_ub(
        Tair,
        Ts,
        soilprofile,
        tspan;
        nfactor=NFactor(nf,nt),
        savevars=(:T,:kc),
        step_limiter=nothing,
        samplerate=Day(1),
    );
end

const bayelva_station_coords = (lat=78.92083, lon=11.83421)

sim_tspan = (DateTime(1999,1,1), DateTime(2023,12,31))

bayelva_obs_lv2 = load_lv2_lto_data("data/input/Bayelva_level2.dat", timedim=:Datetime_UTC)
Ts_tspan = as_dimarray(bayelva_obs_lv2, r"Ts.*")[Ti(sim_tspan[1]..sim_tspan[2])]
Ts_tspan_daily = mean.(DimensionalData.groupby(ustrip.(Ts_tspan), Ti => Date, Z => identity))
bayelva_obs_era5 = DataFrame(CSV.File("data/product/bayelva_era5_Tair_precip_1998-2023.csv"))
# read wgen output from HDF5 file
bayelva_wgen_sims = Dict()
h5open("data/product/bayelva_wgen_glm_v3_seed=1234.hdf5") do hdf
    for k in keys(hdf)
        bayelva_wgen_sims[k] = Array(hdf[k])
    end
end

wgen_timestamps = map(eachcol(bayelva_wgen_sims["time"][:,:,1])) do (i, yr, m, doy)
    date = Date(yr, m)
    return DateTime(yr, m, doy - dayofyear(date) + 1)
end

Impute.impute!(Ts_tspan.data, Impute.Interpolate())
Impute.impute!(Ts_tspan_daily.data, Impute.Interpolate())
Tair_obs = DimArray(bayelva_obs_era5.Tair_mean*u"°C", (Ti(DateTime.(bayelva_obs_era5.UTC)),))
Tair_sim = DimArray(Float64.(bayelva_wgen_sims["Tavg"][1,:,:])*u"°C", (Ti(wgen_timestamps),Dim{:ens}(1:size(bayelva_wgen_sims["Tavg"],3))))
prec_sim = DimArray(Float64.(bayelva_wgen_sims["prec"][1,:,:])*u"mm/d", (Ti(wgen_timestamps),Dim{:ens}(1:size(bayelva_wgen_sims["prec"],3))))
Tsurf_daily = Ts_tspan_daily[Z(1),Ti(sim_tspan[1]..sim_tspan[2])]
Tair_tspan = Tair_obs[Ti(At(collect(dims(Tsurf_daily,Ti))))]
nf = sum(ustrip.(Tsurf_daily[Tair_tspan .<= 0.0u"°C"])) / sum(ustrip.(Tair_tspan[Tair_tspan .<= 0.0u"°C"]))
nt = sum(ustrip.(Tsurf_daily[Tair_tspan .> 0.0u"°C"])) / sum(ustrip.(Tair_tspan[Tair_tspan .> 0.0u"°C"]))
soilprofile = soil_layers_bayelva(dims(Ts_tspan, Z)[1], 5.0u"W/m/K")

end

forward_prob_ref, tile, p = make_forward_prob(
    Tair_obs,
    float.(Ts_tspan),
    soilprofile,
    sim_tspan;
    nf, nt
)
forward_sol_ref = @time solve(forward_prob_ref, LiteImplicitEuler(), dt=24*3600.0, saveat=24*3600.0);
out_ref = CryoGridOutput(forward_sol_ref.sol)
let fig = Makie.Figure(),
    ax = Makie.Axis(fig[1,1]);
    Makie.lines!(ax, ustrip.(out_ref.T[Z(Near(1.0u"m"))]))
    # Makie.lines!(ax, ustrip.(Ts_tspan[Z(Near(1.0u"m"))]))
    fig
end

fdd = freezing_degree_days.(
    DimensionalData.groupby(Ts_tspan_daily[Z(1)], Ti => year),
    DimensionalData.groupby(ustrip.(Tair_tspan), Ti => year)
)

tdd = freezing_degree_days.(
    DimensionalData.groupby(Ts_tspan_daily[Z(1)], Ti => year),
    DimensionalData.groupby(ustrip.(Tair_tspan), Ti => year)
)

@everywhere function prob_func(prob, i, iter)
    prob, tile, p = make_forward_prob(
        Tair_sim[:,i],
        float.(Ts_tspan),
        soilprofile,
        sim_tspan;
        nf, nt
    )
    return prob
end

ensprob = EnsembleProblem(forward_prob_ref; prob_func, output_func=(sol,i) -> (get_observable(sol, :Ts), false))
enssol = solve(ensprob, LiteImplicitEuler(), EnsembleDistributed(), trajectories=size(Tair_sim,2), dt=24*3600.0)
Ts_all = foldl((x,y) -> cat(x, y, dims=:ens), map((i,x) -> DimArray(reshape(x.data, size(x)..., 1), (dims(x)..., Dim{:ens}(i:i))), 1:length(enssol), enssol))

function compute_ALT(Ts)
    alt = Diagnostics.active_layer_thickness(Ts)
    return DimArray(alt.data, (dims(alt, Ti),))
end

alt_ref = compute_ALT(out_ref.T)
alt_ens = foldl((x,y) -> cat(x,y,dims=:ens), map(i -> compute_ALT(Ts_all[ens=i]*u"°C"), 1:size(Ts_all,:ens)))

let fig = Makie.Figure(),
    ax = Makie.Axis(fig[1,1]);
    Makie.lines!(ax, ustrip.(alt_ref.data))
    # Makie.series!(ax, ustrip.(alt_ens.data)', solid_color=(:black,0.2))
    fig
end

Makie.lines(Ts_all[Z=Near(1.75u"m"),ens=10])

Makie.lines(alt_ens[ens=10])