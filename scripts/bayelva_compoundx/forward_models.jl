using Dates

using CryoGrid
using CryoGrid.LiteImplicit
using FreezeCurves
using SimulationBasedInference

default_grid(ztop) = Grid(vcat(ztop:0.02u"m":-0.02u"m", CryoGrid.Presets.DefaultGrid_2cm))

tempprofile(initialT::AbstractDimArray) = TemperatureProfile((z => initialT[Z(At(z))] for z in dims(initialT, Z))...)

soil_layers_bayelva(ztop=0.0u"m", kh_m=4.0u"W/m/K") = SoilProfile(
    uconvert(u"m", ztop) => SimpleSoil(org=0.01, por=Param(0.45), heat=SoilThermalProperties(SimpleSoil, kh_m=Param(ustrip(kh_m))), freezecurve=PainterKarra(swrc=VanGenuchten(:siltloam))),
    0.1u"m" => SimpleSoil(org=0.01, por=Param(0.4), heat=SoilThermalProperties(SimpleSoil, kh_m=Param(ustrip(kh_m))), freezecurve=PainterKarra(swrc=VanGenuchten(:sandyclay))),
    0.5u"m" => SimpleSoil(org=0.01, por=Param(0.3), heat=SoilThermalProperties(SimpleSoil, kh_m=Param(ustrip(kh_m))), freezecurve=PainterKarra(swrc=VanGenuchten(:sandyclay))),
    1.0u"m" => SimpleSoil(org=0.0, por=Param(0.3), heat=SoilThermalProperties(SimpleSoil, kh_m=Param(ustrip(kh_m))), freezecurve=PainterKarra(swrc=VanGenuchten(:siltloam))),
    10.0u"m" => SimpleSoil(org=0.0, por=Param(0.2), heat=SoilThermalProperties(SimpleSoil, kh_m=Param(ustrip(kh_m))), freezecurve=PainterKarra(swrc=VanGenuchten(:siltloam))),
)

function soil_heat_model_Ts_ub(
    Ts_data::DimArray{TT,2},
    soilprofile::SoilProfile,
    tspan::NTuple{2,DateTime};
    heat=HeatBalance(),
    Qgeo=0.05u"W/m^2",
    zbot=1000.0u"m",
    discretization=default_grid(soilprofile[1][1]),
    resolution=Hour(3),
    samplerate=Hour(1),
    prob_kwargs...,
) where {TT<:typeof(0.0u"°C")}
    T0 = TemperatureProfile(
        map(z -> z => Ts_data[Z(At(z)), Ti(At(tspan[1]))], dims(Ts_data, Z))...,
        zbot => Param(5.0, units=u"°C"),
    )
    initT = initializer(:T, T0)
    Ts_ub = InterpolatedForcing(collect(dims(Ts_data, Ti)), float.(Ts_data[Z(1)].data), :Ts_ub)
    forcings = Forcings((; Ts_ub))
    upperbc = TemperatureBC(forcings.Ts_ub)
    lowerbc = GeothermalHeatFlux(Param(ustrip(Qgeo), units=unit(Qgeo)))
    strat = Stratigraphy(
        soilprofile[1][1] => Top(upperbc),
        map(para -> Ground(; heat, para), soilprofile),
        zbot => Bottom(lowerbc),
    )
    @info "Building tile"
    tile = Tile(strat, discretization, initT)
    p = CryoGrid.parameters(tile)
    @info "Evaluating initial condition"
    u0, _ = initialcondition!(tile, tspan, p)
    print(collect(zip(cells(tile.grid), CryoGrid.getvar(:T, tile, u0))))
    @info "Setting up forward problem"
    prob = CryoGridProblem(tile, u0, tspan, p; prob_kwargs...)
    Ts_obs = TemperatureProfileObservable(
        :Ts,
        uconvert.(u"m", collect(dims(Ts_data, Z)[2:end])),
        tspan,
        resolution;
        samplerate,
    )
    forward_prob = SimulatorForwardProblem(prob, Ts_obs)
    return forward_prob, tile, p
end

function soil_heat_model_Tair_ub(
    Tair_data::DimVector,
    Ts_data::DimMatrix,
    soilprofile::SoilProfile,
    tspan::NTuple{2,DateTime};
    heat=HeatBalance(Heat.EnthalpyImplicit()),
    nfactor=NFactor(0.6,0.9),
    Qgeo=0.05u"W/m^2",
    ztop=0.0u"m",
    zbot=1000.0u"m",
    discretization=default_grid(ztop),
    resolution=Day(1),
    samplerate=Hour(1),
    prob_kwargs...,
)
    T0 = filter(x -> !ismissing(x[2]), map(z -> z => Ts_data[Z(At(z)), Ti(At(tspan[1]))], dims(Ts_data, Z)))
    initT_profile = TemperatureProfile(
        T0...,
        zbot => Param(5.0, units=u"°C"),
    )
    Tair = InterpolatedForcing(DateTime.(dims(Tair_data,Ti)), Tair_data.data, :Tair)
    initT = initializer(:T, initT_profile)
    forcings = Forcings((; Tair))
    upperbc = TemperatureBC(forcings.Tair, nfactor)
    lowerbc = GeothermalHeatFlux(Param(ustrip(Qgeo), units=unit(Qgeo)))
    strat = Stratigraphy(
        ztop => Top(upperbc),
        map(para -> Ground(; heat, para), soilprofile),
        zbot => Bottom(lowerbc),
    )
    @info "Building tile"
    tile = Tile(strat, discretization, initT)
    p = CryoGrid.parameters(tile)
    @info "Evaluating initial condition"
    u0, _ = initialcondition!(tile, tspan, p)
    @info "Setting up forward problem"
    prob = CryoGridProblem(tile, u0, tspan, p; prob_kwargs...)
    Ts_obs = TemperatureProfileObservable(
        :Ts,
        uconvert.(u"m", collect(dims(Ts_data, Z)[2:end])),
        tspan,
        resolution;
        samplerate,
    )
    forward_prob = SimulatorForwardProblem(prob, Ts_obs)
    return forward_prob, tile, p
end

function soil_snow_heat_model_Tair_ub(
    Tair_data::DimArray{TT,1},
    prec_data::DimArray{TP,1},
    Ts_data::DimArray{TT,2},
    soilprofile::SoilProfile,
    tspan::NTuple{2,DateTime};
    heat=HeatBalance(Heat.EnthalpyImplicit()),
    snow=Snowpack(Snow.LiteGridded(); heat),
    Qgeo=0.05u"W/m^2",
    ztop=-2.0u"m",
    zbot=1000.0u"m",
    discretization=default_grid(ztop),
    resolution=Day(1),
    samplerate=Hour(1),
    prob_kwargs...,
) where {TT<:typeof(0.0u"°C"),TP<:typeof(0.0u"m/s")}
    T0 = TemperatureProfile(
        map(z -> z => Ts_data[Z(At(z)), Ti(At(tspan[1]))], dims(Ts_data, Z)[2:end])...,
        zbot => Param(5.0, units=u"°C"),
    )
    Tair = InterpolatedForcing(DateTime.(dims(Tair_data,Ti)), Tair_data.data, :Tair)
    precip = InterpolatedForcing(DateTime.(dims(prec_data,Ti)), prec_data.data, :prec)
    initT = initializer(:T, T0)
    forcings = Forcings((; Tair, precip))
    upperbc = WaterHeatBC(
        SurfaceWaterBalance(forcings),
        TemperatureBC(forcings.Tair),
    )
    lowerbc = GeothermalHeatFlux(Param(ustrip(Qgeo), units=unit(Qgeo)))
    strat = @Stratigraphy(
        ztop => Top(upperbc),
        ztop => snow,
        map(para -> Ground(; heat, para), soilprofile)...,
        zbot => Bottom(lowerbc),
    )
    @info "Building tile"
    tile = Tile(strat, discretization, initT)
    p = CryoGrid.parameters(tile)
    @info "Evaluating initial condition"
    u0, _ = initialcondition!(tile, tspan, p)
    @info "Setting up forward problem"
    prob = CryoGridProblem(tile, u0, tspan, p; prob_kwargs...)
    Ts_obs = TemperatureProfileObservable(
        :Ts,
        uconvert.(u"m", collect(dims(Ts_data, Z)[2:end])),
        tspan,
        resolution;
        samplerate,
    )
    forward_prob = SimulatorForwardProblem(prob, Ts_obs)
    return forward_prob, tile, p
end

