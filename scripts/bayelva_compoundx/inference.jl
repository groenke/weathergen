using SimulationBasedInference
using Turing

const default_config = Dict(
    "sigma_km" => 1.0,
    "sigma_por" => 0.5,
    "sigma_org" => 0.5,
    "sigma_Qgeo" => 10.0,
)

@model function priormodel(::Soil, depth, p, config::Dict=default_config, ::Type{T}=Float64) where {T}
    σ_km = config["sigma_km"]
    σ_por = config["sigma_por"]
    σ_org = config["sigma_org"]
    km ~ Normal(p.kh_m[1], σ_km)
    # ensure that km > 0.1 (generally shouldn't happen)
    km = max(0.1*one(km), km)
    # use logit normal prior with median on default value
    por ~ LogitNormal(logit(p.por[1]), σ_por)
    if haskey(p, :org)
        org ~ LogitNormal(logit(p.org[1]), σ_org)
        return vcat(por, org, km)
    end
    return vcat(por, km)
end

@model function priormodel(tile::Tile, p0, config::Dict=default_config, ::Type{T}=Float64) where {T}
    p = similar(p0, T)
    copyto!(p, p0)
    # soil layer priors
    for (depth, layer) in zip(boundaries(tile.strat), namedlayers(tile.strat)[2:end-1])
        nm = nameof(layer)
        if haskey(p0, nm)
            p[nm] .= @submodel prefix="$(nameof(layer))" priormodel(layer.val, depth, p0[nm], config)
        end
    end
    σ_Qgeo = config["sigma_Qgeo"]
    Qgeo ~ Normal(p0.bottom.Qgeo[1]*1000, σ_Qgeo)
    p.bottom.Qgeo .= Qgeo/100
    return p
end
