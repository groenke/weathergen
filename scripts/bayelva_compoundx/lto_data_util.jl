using DataFrames, CSV
using Dates
using DimensionalData
using Statistics
using TimeSeries
using Unitful

struct MetsoilDataset
    df::DataFrame
end

Base.getindex(ms::MetsoilDataset, var) = select(ms.df, var)

get_timestamps(ms::MetsoilDataset) = ms.df.UTC

function as_dimarray(ms::MetsoilDataset, vars)
    df_sub = select(ms.df, vars)
    ts = ms.df.UTC
    cols = names(df_sub)
    depthstrings = map(nm -> split(nm, "_")[end], cols)
    depths = float.(uparse.(depthstrings))
    return DimArray(Array(df_sub), (Ti(ts),Z(depths)))
end

function parse_units(name, x)
    pts = split(name, "_")
    unit = uparse(replace(pts[2], "deg" => "°", "2" => "^2", "3" => "^3", "hour" => "hr"))
    return x.*unit
end

function selectors(df)
    colnames = names(df)
    return map(colnames) do col
        col => x -> parse_units(col, x)
    end
end

function get_interp_mode(varname)
    if varname ∈ (:prec_total, :prec_liquid, :Dsn)
        return Constant()
    else
        return Linear()
    end
end

function load_time_series_from_csv(filepath; timedim="UTC", timeformat="YYY-mm-dd HH:MM")
    df = DataFrame(CSV.File(filepath, comment="#", missingstring="NA"))
    if eltype(df[:,timedim]) <: TimeType
        timestamps = DateTime.(df[:,timedim])
    else
        timestamps = DateTime.(df[:,timedim], timeformat)
    end
    data = select(df, Not(timedim))
    tarray = TimeArray(timestamps, Array(data), names(data))
    return tarray
end


function load_lv2_lto_data(filepath; timedim=:UTC, timeformat="YYY-mm-dd HH:MM")
    df = DataFrame(CSV.File(filepath, comment="#", missingstring="NA"))
    df_sub = select(df, timedim => ByRow(t -> DateTime(t, timeformat)) => :UTC, selectors(select(df, Not(timedim)))..., renamecols=false)
    return MetsoilDataset(df_sub)
end
