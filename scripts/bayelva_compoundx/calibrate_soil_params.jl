using Distributed
using Impute
using CSV, DataFrames, Rasters, TimeSeries
using Statistics

using Turing

# plotting
import CairoMakie as Makie

const project_dir = dirname(Base.active_project())

addprocs(exeflags=["--project=$(project_dir)"])

@everywhere include("lto_data_util.jl")
@everywhere include("forward_models.jl")
@everywhere include("inference.jl")

const bayelva_station_coords = (lat=78.92083, lon=11.83421)

sim_tspan = (DateTime(2013,1,1), DateTime(2013,2,1))

bayelva_dataset = load_lv2_lto_data("data/input/Bayelva_level2.dat", timedim=:Datetime_UTC)
vwc_all = as_dimarray(bayelva_dataset, r"VWC.*")
Ts_tspan = as_dimarray(bayelva_dataset, r"Ts.*")[Ti(sim_tspan[1]..sim_tspan[2]+Hour(1))]
Ts_tspan_daily = mean.(DimensionalData.groupby(ustrip.(Ts_tspan), Ti => Date, Z => identity))
# bayelva_era5_df = DataFrame(CSV.File("data/product/bayelva_era5_Tair_precip_1998-2023.csv"))
# bayelva_forcing_data = DimStack((;
#     Tair=DimArray(bayelva_era5_df.Tair_mean, (Ti(bayelva_era5_df.UTC),)),
#     prec=DimArray(ustrip.(u"m/s", bayelva_era5_df.prec*u"mm/d"), (Ti(bayelva_era5_df.UTC),)),
# ))

Impute.impute!(Ts_tspan.data, Impute.Interpolate())
soilprofile = soil_layers_bayelva(dims(Ts_tspan, Z)[1])
forward_prob, tile, p = soil_heat_model_Ts_ub(
    float.(Ts_tspan), # convert to non-missing floating point type
    soilprofile,
    sim_tspan;
    savevars=(:T,:kc),
    step_limiter=nothing,
);

forward_sol = @time solve(forward_prob, SSPRK43(), dt=180.0, saveat=300.0);
out = CryoGridOutput(forward_sol.sol)

param_prior = SBI.prior(priormodel(tile, ustrip.(vec(p))));
# extract data, dropping first sensor depth
Ts_data = Rasters.aggregate(mean, Raster(ustrip.(Ts_tspan)), (Ti(3),))
noise_prior = Exponential(0.1)
Ts_lik = SimulatorLikelihood(IsoNormal, forward_prob.observables.Ts, Array(Ts_data[:,2:end])', noise_prior)
inference_prob = SimulatorInferenceProblem(forward_prob, SSPRK43(), param_prior, Ts_lik)

let z_obs = 10.0u"cm",
    Ts_pred = get_observable(forward_sol, :Ts)[Z(Near(z_obs))];
    fig = Makie.lines(ustrip.(Ts_data[Z(1)]))
    Makie.lines!(fig.axis, ustrip.(Ts_data[Z(At(z_obs))]))
    Makie.lines!(fig.axis, Ts_pred)
    fig
end

solver = init(inference_prob, ESMDA(), EnsembleDistributed(), dt=300.0, ensemble_size=64);
# test one iteration
@time step!(solver)
# run until completion
esmda_sol = solve!(solver)