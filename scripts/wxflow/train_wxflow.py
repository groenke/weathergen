# %%
import argparse
import os
import os.path as path
import sys
os.chdir(path.dirname(__file__))
sys.path.append("../../src/python")

import logging
from tqdm import tqdm

# data loading
import pandas as pd
import numpy as np

import torch
import torch.distributions as dist
import torch.optim.lr_scheduler as lrs
import zuko

import matplotlib.pyplot as plt

from weathergen.utils import fourier_lsq, fourier_feats
from weathergen.data import EOBSBasinDataset

parser = argparse.ArgumentParser(
    prog='wxflow-train',
    description='Train a conditional normalizing flow weather generator'
)
parser.add_argument('-t', "--transforms", default=8, type=int)
parser.add_argument('-c', "--components", default=32, type=int)
parser.add_argument('-e', "--epochs", default=50, type=int)
args = vars(parser.parse_args())

gpu_id = 0
device = torch.device(f"cuda:{gpu_id}" if torch.cuda.is_available() else "cpu")

print(f"using device: {device}")

DATA_DIR = "/data/compoundx/causal_flood/basins_averaged"

# %%
def torch_tavg_summary_stats(Tair):
    Tair90 = torch.quantile(Tair, 0.99, axis=0, keepdim=True)
    Tair50 = torch.quantile(Tair, 0.50, axis=0, keepdim=True)
    Tair10 = torch.quantile(Tair, 0.01, axis=0, keepdim=True)
    tdd = torch.mean(torch.where(Tair > 0, Tair, 0), axis=0, keepdim=True)
    fdd = torch.mean(torch.where(Tair <= 0, Tair, 0), axis=0, keepdim=True)
    return torch.cat([Tair10, Tair50, Tair90, tdd, fdd])

def tavg_summary_stats(df):
    T = df.tavg.values
    stats = torch_tavg_summary_stats(torch.from_numpy(T))
    return pd.DataFrame(stats.numpy().reshape((1,-1)), columns=["T10","T50","T90","TDD","FDD"])

def save_model(flow, errors, args):
    os.makedirs("../../data/models/", exist_ok=True)
    filepath = f"../../data/models/basin_flow_DE_transforms={args['transforms']}_components={args['components']}.pt"
    if path.isfile(filepath):
        os.remove(filepath)
    f = torch.save({'state': flow.state_dict(), 'error': errors}, filepath)
    return f

# %%
basins = []
basin_info = pd.read_csv(path.join(DATA_DIR, "basins_info.csv"))
basin_info = basin_info.loc[basin_info.country == "DE"]
dataset = EOBSBasinDataset(basin_info, path.join(DATA_DIR, "observational"), summarizer=tavg_summary_stats)

# %%
basin_idx = 0
all_basins = dataset.dataset
all_basins.loc[all_basins.id == all_basins.id.unique()[basin_idx]]
all_basins["time"] = pd.to_datetime(all_basins["time"])

# %%
context_dims = len(dataset.basin_features) + dataset.summary_stats.shape[1]-2
flow = zuko.flows.GF(365, context_dims, transforms=args['transforms'], components=args['components']).to(device)
# flow = zuko.flows.core.Flow(??)
list(flow.parameters())[0].dtype

# %%
num_epochs = args['epochs']
optimizer = torch.optim.Adam(flow.parameters(), lr=1e-3)
scheduler = lrs.CosineAnnealingLR(optimizer, T_max=num_epochs, eta_min=1e-4)
y_standardizer = dataset.standardizer["data"]
stats_standardizer = dataset.standardizer["stats"]
y_shift = torch.from_numpy(y_standardizer["shift"][["tavg"]].values.astype(np.float32)).to(device)
y_scale = torch.from_numpy(y_standardizer["scale"][["tavg"]].values.astype(np.float32)).to(device)
s_shift = torch.from_numpy(stats_standardizer["shift"].values.astype(np.float32)).to(device)
s_scale = torch.from_numpy(stats_standardizer["scale"].values.astype(np.float32)).to(device)

# %%
f0, d0, s0 = dataset[0]
year_inds = d0.groupby(d0.time.dt.year).indices
x_i = torch.from_numpy(dataset.standardize(f0, "features").values.astype(np.float32).reshape((1,-1))).to(device)
y_i = torch.from_numpy(dataset.standardize(d0.iloc[:,1:], "data")[["tavg"]].values.astype(np.float32)).to(device)
s_i = torch.from_numpy(dataset.standardize(s0, "stats").values.astype(np.float32)).to(device)
c_i = torch.concat([x_i*torch.ones((s_i.shape[0],1)).to(device), s_i], axis=1)
y_i_years = [y_i[idx,:][:365,:].T for idx in year_inds.values()]
s_i_scaled = s_i*s_scale.reshape((1,-1)) + s_shift.reshape((1,-1))
y_ij = torch.concat(y_i_years, axis=0)
y_ij_scaled = y_ij*y_scale + y_shift
assert y_ij.shape[0] == c_i.shape[0] == s_i.shape[0]
cond_flow = flow(c_i)
nll = -cond_flow.log_prob(y_ij).mean()
y_sim = cond_flow.sample()*y_scale.reshape((1,-1)) + y_shift.reshape((1,-1))
s_sim = torch_tavg_summary_stats(y_sim.T)
s_err = torch.mean((s_sim.T - s_i_scaled)**2)

# %%
err_dict = dict()
for epoch in range(num_epochs):
    print(f"Starting epoch {epoch+1}/{num_epochs}")
    nlls = []
    serrs = []
    for i in tqdm(range(len(dataset))):
        # load data for basin i
        feats_i, data_i, stats_i = dataset[i]
        year_inds = data_i.groupby(data_i.time.dt.year).indices
        x_i = torch.from_numpy(dataset.standardize(feats_i, "features").values.astype(np.float32).reshape((1,-1))).to(device)
        y_i = torch.from_numpy(dataset.standardize(data_i.iloc[:,1:], "data")[["tavg"]].values.astype(np.float32)).to(device)
        s_i = torch.from_numpy(dataset.standardize(stats_i, "stats").values.astype(np.float32)).to(device)
        c_i = torch.concat([x_i*torch.ones((s_i.shape[0],1)).to(device), s_i], axis=1)
        s_i_scaled = s_i*s_scale.reshape((1,-1)) + s_shift.reshape((1,-1))
        torch._assert(~torch.isnan(x_i).any(), "NaN detected in x_i")
        torch._assert(~torch.isnan(y_i).any(), "NaN detected in y_i")
        torch._assert(~torch.isnan(s_i).any(), "NaN detected in s_i")
        y_i_years = [y_i[idx,:][:365,:].T for idx in year_inds.values()]
        y_ij = torch.concat(y_i_years, axis=0)
        assert y_ij.shape[0] == c_i.shape[0] == s_i.shape[0]
        cond_flow = flow(c_i)
        nll = -cond_flow.log_prob(y_ij).mean()
        y_sim = cond_flow.sample()*y_scale.reshape((1,-1)) + y_shift.reshape((1,-1))
        s_sim = torch_tavg_summary_stats(y_sim.T)
        s_err = torch.mean((s_sim.T - s_i_scaled)**2)
        loss = nll + s_err
        torch._assert(~loss.isnan().any(), f"NaN loss detected in basin {i}, epoch {epoch}")
        loss.backward()
        optimizer.step()
        optimizer.zero_grad()
        nlls.append(nll.detach())
        serrs.append(s_err.detach())
    scheduler.step()
    nll_avg = torch.stack(nlls).mean().to(torch.device("cpu"))
    serr_avg = torch.stack(serrs).mean().to(torch.device("cpu"))
    err_dict[epoch] = {'nll': nll_avg, 'stats': serr_avg}
    save_model(flow, err_dict, args)
    print(f"Finished epoch {epoch+1}; avg nll = {nll_avg}, avg stats err = {torch.sqrt(serr_avg)}")

# %%
