using WeatherGen

# inference
using Gen
using PythonCall
using SimulationBasedInference
using SimulationBasedInference.PySBI

# data processing
using CSV, DataFrames
using DataFramesMeta: @transform, @subset
using Impute
using TimeSeries

# plotting
import CairoMakie as Makie

# rng
import Random
const rng = Random.MersenneTwister(1234)

# read basin data
basin_id = 6340800
basin_metadata = DataFrame(CSV.File("data/input/basins_info.csv"))
basin_data = DataFrame(CSV.File("data/input/$basin_id.csv"))
basin_data = @transform(basin_data, :year = year.(:time), :month = month.(:time))
basin_para = DataFrame(CSV.File("data/product/basin_para_monthly_$basin_id.csv"))
basin_para = @transform(basin_para, :year = year.(:time), :month = month.(:time))
reference_period = (1950, 1980)
data_ref = @subset basin_data :year .>= reference_period[1] .&& :year .<= reference_period[2]
basin_ref_para = @subset basin_para :year .>= reference_period[1] .&& :year .<= reference_period[2]
tavg_dry_std = combine(groupby(basin_ref_para, :month), :tavg_dry => std)
tavg_wet_std = combine(groupby(basin_ref_para, :month), :tavg_wet => std)

function summary_stats(ts, precip, Tair)
    prec_stats = precip_summary_stats(ts, precip)
    tair_stats = tair_summary_stats(ts, Tair)
    return vcat(prec_stats, tair_stats)
end

wgen_para = WGENPara(
    prec = WGENPrecip(
        pdd = parameterize_monthly_hierarchical(UnitParam, basin_ref_para.time, basin_ref_para.pdd),
        pwd = parameterize_monthly_hierarchical(UnitParam, basin_ref_para.time, basin_ref_para.pwd),
        gamma_mean = parameterize_monthly_hierarchical(PositiveParam, basin_ref_para.time, basin_ref_para.prec_avg),
    ),
    Tair = WGENAirTemp(
        mean = parameterize_monthly_hierarchical(LocScaleParam, basin_ref_para.time, basin_ref_para.tavg),
        min = parameterize_monthly_hierarchical(LocScaleParam, basin_ref_para.time, basin_ref_para.tmin),
        max = parameterize_monthly_hierarchical(LocScaleParam, basin_ref_para.time, basin_ref_para.tmax),
    )
)
trace = simulate(wgen_prior, (wgen_para,))
genprior = prior(wgen_prior, (wgen_para,))
θ₀ = genprior(rand(genprior))
Nt = 365

# stats_obs = ComponentVector(p99=50.0, pmean=4.0, ptot=250.0, wd=85.0, cdd=3.5)
basin_data_sub = filter(row -> year(row.time) == 1980, basin_data)
initialstate = WGENState(t=Date(basin_data_sub.time[1]-Day(1)))
stats_obs = summary_stats(basin_data_sub.time, basin_data_sub.pre, basin_data_sub.tavg)
precip_sim = SimulatorObservable(:prec, sol -> WeatherGen.get_precip_from(sol.u), (Nt,))
tair_sim = SimulatorObservable(:tair, sol -> WeatherGen.get_airtemp_from(sol.u), (Nt,))
stats_sim = SimulatorObservable(:stats, sol -> summary_stats(WeatherGen.get_timestamps_from(sol.u), WeatherGen.get_precip_from(sol.u), map(T -> T.mean, WeatherGen.get_airtemp_from(sol.u))), (length(stats_obs),))
forward_prob = SimulatorForwardProblem(wgen_chain, (initialstate, wgen_para, Nt), θ₀, precip_sim, tair_sim, stats_sim)

# test forward solve
forward_sol = @time solve(forward_prob, p=θ₀);
precip_sim = get_observable(forward_sol, :prec)
Makie.lines(precip_sim)
Tair_min_sim = map(Tair -> Tair.min, get_observable(forward_sol, :tair))
Tair_mean_sim = map(Tair -> Tair.mean, get_observable(forward_sol, :tair))
Tair_max_sim = map(Tair -> Tair.max, get_observable(forward_sol, :tair))
Makie.series([Tair_min_sim Tair_mean_sim Tair_max_sim]')
Makie.series([basin_data_sub.tmin basin_data_sub.tavg basin_data_sub.tmax]')

@assert all(Tair_max_sim .> Tair_mean_sim .> Tair_min_sim)

# prior predictive
prior_samples = reduce(hcat, rand(rng, genprior, 1000))
prior_preds_all = map(eachcol(prior_samples)) do x
    forward_sol = solve(forward_prob, p=genprior(x))
    prec = WeatherGen.get_precip_from(forward_sol.sol)
    Tair = WeatherGen.get_airtemp_from(forward_sol.sol)
    ts = WeatherGen.get_timestamps_from(forward_sol.sol)
    summary_stats(ts, prec, map(T -> T.mean, Tair))
end
prior_preds = reduce(hcat, prior_preds_all)
@assert all(isfinite.(prior_preds))

let fig = Makie.Figure();
    for (i,j) in enumerate(collect(first(axes(stats_obs)))[1:5])
        ax_i = Makie.Axis(fig[i,1], title=string(keys(stats_obs)[j]))
        Makie.density!(ax_i, prior_preds[j,:])
        Makie.vlines!(ax_i, stats_obs[j:j], linestyle=:dash, color=:black)
    end
    fig
end

# posterior inference
# stats_obs = prior_preds_all[1]
implicit_lik = ImplicitLikelihood(forward_prob.observables.stats, stats_obs)
inference_prob = SimulatorInferenceProblem(forward_prob, genprior, implicit_lik)
inference_sol = solve(inference_prob, PySNE(), num_simulations=10_000)

# draw posterior samples and generate predictions
posterior_samples = Float64.(sample(rng, inference_sol.result, 1000))
posterior_mean = ComponentArray(mean(posterior_samples, dims=2)[:,1], genprior.axes)
posterior_std = ComponentArray(std(posterior_samples, dims=2)[:,1], genprior.axes)
posterior_trajectories = map(eachcol(posterior_samples)) do x
    sol = solve(forward_prob, p=genprior(x))
    precip = WeatherGen.get_precip_from(sol.sol)
    tair = WeatherGen.get_airtemp_from(sol.sol)
    return precip, tair
end
precip_samples = reduce(hcat, map(first, posterior_trajectories))
tair_samples = reduce(hcat, map(last, posterior_trajectories))
tair_mean_samples = getproperty.(tair_samples, :mean)
tair_min_samples = getproperty.(tair_samples, :min)
tair_max_samples = getproperty.(tair_samples, :max)
@assert all(tair_min_samples[:,1] .< tair_mean_samples[:,1] .< tair_max_samples[:,1])

ts = initialstate.t+Day(1):Day(1):initialstate.t+Year(1)
pred_all = reduce(hcat, map((pr,tair) -> summary_stats(ts, pr, map(T -> T.mean, tair)), eachcol(precip_samples), eachcol(tair_samples)))
pred_mean = mean(pred_all, dims=2)
pred_std = std(pred_all, dims=2)
mode_idx = argmin(sum(Array(abs.(pred_all .- stats_obs)), dims=1)[1,:])
pred_mode = pred_all[:,mode_idx]
pred_mean[:,1] .- stats_obs
pred_mode
stats_obs

let fig = Makie.Figure(size=(800,600));
    Makie.Label(fig[1,2], "Posterior predictive")
    for (i, j) in enumerate(collect(first(axes(stats_obs)))[1:5])
        ax_i = Makie.Axis(fig[i+1,1:3], title=string(keys(stats_obs)[j]))
        d1 = Makie.density!(ax_i, prior_preds[j,:], color=(:gray,0.5))
        d2 = Makie.density!(ax_i, pred_all[j,:], color=(:red,0.5))
        Makie.vlines!(ax_i, stats_obs[j:j], linestyle=:dash, color=:black)
    end 
    # Makie.save("plots/wgen_precip_posterior_predictive_snpe.png", fig)
    fig
end

let fig = Makie.Figure(size=(800,600));
    Makie.Label(fig[1,2], "Posterior predictive")
    for (i, j) in enumerate(collect(first(axes(stats_obs)))[6:end])
        ax_i = Makie.Axis(fig[i+1,1:3], title=string(keys(stats_obs)[j]))
        d1 = Makie.density!(ax_i, prior_preds[j,:], color=(:gray,0.5))
        d2 = Makie.density!(ax_i, pred_all[j,:], color=(:red,0.5))
        Makie.vlines!(ax_i, stats_obs[j:j], linestyle=:dash, color=:black)
    end 
    # Makie.save("plots/wgen_precip_posterior_predictive_snpe.png", fig)
    fig
end

# assess internal variability
N = 1000
posterior_mode = posterior_samples[:,mode_idx]
rep_samples = map(1:N) do i
    prec = WeatherGen.get_precip_from(solve(forward_prob, p=genprior(posterior_mode)).sol)
    precip_summary_stats(ts, prec)
end
rep_samples = reduce(hcat, rep_samples)

let fig = Makie.Figure(size=(800,600));
    Makie.Label(fig[1,2], "Internal variability at mode")
    for i in first(axes(stats_obs))
        ax_i = Makie.Axis(fig[i+1,1:3], title=string(keys(stats_obs)[i]))
        d1 = Makie.density!(ax_i, prior_preds[i,:], color=(:gray,0.5))
        d2 = Makie.density!(ax_i, rep_samples[i,:], color=(:red,0.5))
        Makie.vlines!(ax_i, stats_obs[i:i], linestyle=:dash, color=:black)
    end
    # Makie.Legend(fig[length(stats_obs)+1,2], )
    Makie.save("plots/wgen_precip_posterior_mode_variability_snpe.png", fig)
    fig
end

# more plots

let fig = Makie.Figure(),
    ax = Makie.Axis(fig[1,1], title="Precipitation: simulated vs. observed", ylabel="Precipitation / mm", xlabel="Day of year");
    s = Makie.series!(ax, precip_samples[:,1:10]', solid_color=(:black, 0.3), linewidth=1.0)
    l = Makie.lines!(ax, basin_data_sub.pre, linewidth=1.0, color=:blue)
    Makie.axislegend(ax, [s,l], ["Simulated","Observed (1980)"])
    fig
end

let fig = Makie.Figure(),
    ax = Makie.Axis(fig[1,1], title="Daily mean air temperature: simulated vs. observed", ylabel="Air temperature / °C", xlabel="Day of year");
    s = Makie.series!(ax, tair_mean_samples[:,1:10]', solid_color=(:black, 0.3), linewidth=1.0)
    l = Makie.lines!(ax, basin_data_sub.tavg, linewidth=1.0, color=:blue)
    Makie.axislegend(ax, [s,l], ["Simulated","Observed (1980)"])
    fig
end

Makie.series(tair_min_samples[:,rand(rng, 1:1000, 10)]', solid_color=(:black, 0.3), linewidth=1.0)
Makie.series(tair_max_samples[:,rand(rng, 1:1000, 10)]', solid_color=(:black, 0.3), linewidth=1.0)


let fig = Makie.Figure(),
    ax = Makie.Axis(
        fig[1,1],
        xticks=1:12*2:size(basin_ref_para,1),
        xtickformat=i -> string.(year.(basin_ref_para.time[Int.(i)])),
        xticklabelrotation=π/4,
        ylabel="Probability",
        title="Monthly precipitation occurrence prob. $(reference_period[1])-$(reference_period[2])"
    );
    lpdd = Makie.lines!(ax, basin_ref_para.pdd)
    lpwd = Makie.lines!(ax, basin_ref_para.pwd)
    Makie.axislegend(ax, [lpdd, lpwd], ["Pr(dry|dry)", "Pr(dry|wet)"])
    fig
end

let fig = Makie.Figure(),
    ax1 = Makie.Axis(fig[1,1], xticks=1:12, ylabel="probability", title="Pr(dry|dry)"),
    ax2 = Makie.Axis(fig[1,2], xticks=1:12, title="Pr(wet|dry)"),
    ax3 = Makie.Axis(fig[2,1:2], xticks=1:12, title="Average precipitation", ylabel="Precipitation / mm");
    Makie.boxplot!(ax1, month.(basin_ref_para.time), basin_ref_para.pdd)
    Makie.boxplot!(ax2, month.(basin_ref_para.time), basin_ref_para.pwd)
    Makie.boxplot!(ax3, month.(basin_ref_para.time), basin_ref_para.prec_avg)
    Makie.save("plots/basin_$(basin_id)_ref_period_prec.png", fig)
    fig
end

let fig = Makie.Figure(),
    ax = Makie.Axis(fig[1,1]);
    Makie.boxplot!(ax, month.(basin_ref_para.time), basin_ref_para.tavg)
    fig
end
