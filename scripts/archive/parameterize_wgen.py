#%%
import sys
sys.path.append("../../src/python")

#%%
import argparse
import pandas as pd
import numpy as np
import scipy.fft as fft

import matplotlib.pyplot as plt

from weathergen import precip_summary_stats

parser = argparse.ArgumentParser(description="WGEN parameterization script")
parser.add_argument('input_file', required=True)
parser.add_argument('-o', 'output_file', default="wgen_params.csv", type=str)
args = vars(parser.parse_args())

#%%
# met_id = 6340800
met_data = pd.read_csv(args['input_file'])
met_data["time"] = pd.to_datetime(met_data["time"])
met_data = met_data.set_index("time")
met_data.head()

# %%
met_prec = met_data.pre
met_prec_series = met_prec.reset_index().drop(["time"], axis=1)
pdd_ref = (met_prec_series.iloc[:-1].reset_index() <= 0) & (met_prec_series.iloc[1:].reset_index() <= 0)
pdd_ref["time"] = met_prec.index[1:]
pwd_ref = (met_prec_series.iloc[:-1].reset_index() > 0) & (met_prec_series.iloc[1:].reset_index() <= 0)
pwd_ref["time"] = met_prec.index[1:]
pdd_ref_monthly = pdd_ref.drop(["index"], axis=1).set_index("time").resample("1ME").mean()
pwd_ref_monthly = pwd_ref.drop(["index"], axis=1).set_index("time").resample("1ME").mean()
ptot_monthly = met_prec.resample("1ME").sum()
prec_avg_monthly = met_prec[met_prec > 0].resample("1ME").mean()
wet_days_monthly = (met_prec > 0).resample("1ME").sum()
tavg_wet_monthly = met_data.tavg[met_prec > 0].resample("1ME").mean()
tmax_wet_monthly = met_data.tmax[met_prec > 0].resample("1ME").mean()
tmin_wet_monthly = met_data.tmin[met_prec > 0].resample("1ME").mean()
tavg_dry_monthly = met_data.tavg[met_prec <= 0].resample("1ME").mean()
tmax_dry_monthly = met_data.tmax[met_prec <= 0].resample("1ME").mean()
tmin_dry_monthly = met_data.tmin[met_prec <= 0].resample("1ME").mean()

# %%
agg_ops = {'tavg': 'mean', 'tmin': 'mean', 'tmax': 'mean', 'pre': 'sum'}
met_data_monthly = met_data[["tavg","tmin","tmax","pre"]].resample("1ME").aggregate(agg_ops)
met_para_monthly = pd.concat([
    met_data_monthly,
    tavg_wet_monthly.to_frame().rename({'tavg': 'tavg_wet'}, axis=1),
    tmax_wet_monthly.to_frame().rename({'tmax': 'tmax_wet'}, axis=1),
    tmin_wet_monthly.to_frame().rename({'tmin': 'tmin_wet'}, axis=1),
    tavg_dry_monthly.to_frame().rename({'tavg': 'tavg_dry'}, axis=1),
    tmax_dry_monthly.to_frame().rename({'tmax': 'tmax_dry'}, axis=1),
    tmin_dry_monthly.to_frame().rename({'tmin': 'tmin_dry'}, axis=1),
    pdd_ref_monthly.rename({'pre': 'pdd'}, axis=1),
    pwd_ref_monthly.rename({'pre': 'pwd'}, axis=1),
    ptot_monthly.to_frame().rename({'pre': 'ptot'}, axis=1),
    prec_avg_monthly.to_frame().rename({'pre': 'prec_avg'}, axis=1),
    wet_days_monthly.to_frame().rename({'pre': 'wet_days'}, axis=1),
], axis=1)
met_para_monthly.head()
met_para_monthly.to_csv(args['output_file'])

# %%
met_summary_stats = met_prec.groupby(met_prec.index.year).apply(
    lambda prec: pd.DataFrame(np.array(precip_summary_stats(prec.values)).reshape((1,5)), columns=['ptot','p99','pmean','wd','cdd'])
).reset_index().drop(columns=['level_1']).set_index('time')
# met_summary_stats.to_csv(f"../../data/product/met_summary_stats.csv")

#%%
# met_data.pre["1950"].plot()
tavg_autocorr = met_data.tavg.diff().groupby(met_data.index.year).apply(lambda x: x.autocorr())
print(f"{tavg_autocorr.mean()} +/- {tavg_autocorr.std()}")
met_data.tavg["1980"].plot()

# %%
tmax_centered = met_data.tmax - met_data.tmax.mean()
tmin_centered = met_data.tmin - met_data.tmin.mean()
tavg_centered = met_data.tavg - met_data.tavg.mean()
tmax_fft = fft.fft(tmax_centered.values)
tmin_fft = fft.fft(tmin_centered.values)
tavg_fft = fft.fft(tavg_centered.values)
# %%
