# %%
import argparse
import datetime
import sys
import os
sys.path.append("src/python")

# prevent JAX from preallocating all GPU memory
os.environ["XLA_PYTHON_CLIENT_PREALLOCATE"] = "false"

# data handling
import pandas as pd
import numpy as np
import xarray as xr

# IO
import pickle

# plotting
import matplotlib.pyplot as plt
import seaborn as sns

# pyro
import numpyro
import numpyro.distributions as dist
from numpyro.infer import Predictive
from numpyro.infer.autoguide import AutoMultivariateNormal

# jax
import jax
import jax.numpy as jnp
import jax.random as random

import weathergen as wx

from tqdm import tqdm

models = {
    'wgen_glm_v2': wx.wgen_glm_v2,
    'wgen_glm_v3': wx.wgen_glm_v3,
}

DATA_DIR = "/data/compoundx/causal_flood/basins_averaged"

# default args for interactive mode
args = dict(
    index=0,
    outdir="/work/groenke/wgen-glm-eobs/analysis",
    model="wgen_glm_v2",
)

# %%
parser = argparse.ArgumentParser(prog="analyze-wgen-eobs", description="WGEN-GLM E-OBS basin calibration")
parser.add_argument("index", default=0, type=int)
parser.add_argument('-o', "--outdir", default=".")
parser.add_argument('-m', '--model', default="wgen_glm_v2", type=str)

args = vars(parser.parse_args())
print(args)

# %%
basins_info = pd.read_csv(os.path.join(DATA_DIR, "basins_info.csv"))
# 236 basins with >= 30 years of data
basins_info_de = basins_info.loc[np.logical_and(basins_info.country == "DE", basins_info.t_yrs >= 30),:].reset_index()
basin_info = basins_info_de.iloc[args["index"]]
name_map = wx.data.data_var_name_map(prec="pre", Tair_mean="tavg", Tair_min="tmin", Tair_max="tmax")
basin_data = wx.data.load_time_series_csv(os.path.join(DATA_DIR, "observational", f"{basin_info.id}.csv"), name_map)
# if Tair_mean violates sanity checks, replace with midpoint
basin_data["Tair_mean"] = basin_data.Tair_mean.where(
    np.logical_and(basin_data.Tair_mean > basin_data.Tair_min, basin_data.Tair_mean < basin_data.Tair_max),
    (basin_data.Tair_min + basin_data.Tair_max) / 2
)
# set any remaining rows which violate the consistency check to nan
basin_data = basin_data.where(
    np.logical_and(basin_data.Tair_mean > basin_data.Tair_min, basin_data.Tair_mean < basin_data.Tair_max),
    np.nan,                 
)

# %%
print(f"loaded data for basin {basin_info.id} with {basin_data.shape[0]} samples")
print(basin_info)
# assert np.isfinite(basin_data[["Tair_mean","Tair_min","Tair_max","prec"]]).all().all(), "found nan/inf values in dataset"
assert (basin_data.dropna().Tair_max > basin_data.dropna().Tair_min).all(), "found invalid data values; Tair_max <= Tair_min"
assert (basin_data.dropna().Tair_max > basin_data.dropna().Tair_mean).all(), "found invalid data values; Tair_max <= Tair_mean"
assert (basin_data.dropna().Tair_mean > basin_data.dropna().Tair_min).all(), "found invalid data values; Tair_mean <= Tair_min"
# %%
results_dir = f"/work/groenke/wgen-glm-eobs/calibration/{args['model']}"
files = [os.path.join(results_dir, f) for f in os.listdir(results_dir) if str(basin_info.id) in f]
periods = [f.split('.')[0].split('_')[-1].split("-") for f in files]

# %%
model = models[args["model"]]
Tair_freqs = np.array([0.1,0.2,0.5,1.0,2.0,3.0,4.0])/365.25
prec_freqs = np.array([1.0,2.0,3.0,4.0])/365.25
wgen = wx.WGEN(basin_data, model, order=2, Tavg_dof_mean=10.0, Tair_freqs=Tair_freqs, prec_freqs=prec_freqs)
_, year, month, doy = wgen.timestamps[0,:,:].T
timestamps = pd.to_datetime([datetime.datetime(int(year[i]), 1, 1) + datetime.timedelta(int(doy[i])-1) for i in range(len(year))])

# %%
def recenter(data: xr.DataArray, dim='time'):
    return data - data.mean(dim=dim)

def acf(data: xr.Dataset):
    n = len(data.time)
    return data.apply(lambda x: xr.corr(
            x.isel(time=slice(0,-1)).assign_coords(time=np.arange(0,n-1)),
            x.isel(time=slice(1,None)).assign_coords(time=np.arange(0,n-1)),
            dim="time"
        )
    )

def compute_diagnostics(data: xr.Dataset, period, quantiles=np.arange(0.001,0.999,0.001)):
    data_train = data.sel(time=slice(period[0], period[1]))
    # monthly means
    monthly_means = data_train.groupby(data_train.time.dt.month).mean().expand_dims("var", axis=-1)
    monthly_stds = data_train.groupby(data_train.time.dt.month).std().expand_dims("var", axis=-1)
    monthly_min = data_train.groupby(data_train.time.dt.month).min().expand_dims("var", axis=-1)
    monthly_max = data_train.groupby(data_train.time.dt.month).max().expand_dims("var", axis=-1)
    monthly_acfs = data_train.groupby(data_train.time.dt.month).apply(acf).expand_dims("var", axis=-1)
    monthly_all = xr.concat([monthly_means, monthly_stds, monthly_min, monthly_max, monthly_acfs], dim="var").assign_coords(var=["mean","std","min","max","autocorr"])
    # pearson residuals
    pres_prec = (data.prec - data.prec_sim.mean(dim="sample")) / data.prec_sim.std(dim="sample")
    pres_Tavg = (data.Tair_mean - data.Tavg_sim.mean(dim="sample")) / data.Tavg_sim.std(dim="sample")
    pres_Tmin = (data.Tair_min - data.Tmin_sim.mean(dim="sample")) / data.Tmin_sim.std(dim="sample")
    pres_Tmax = (data.Tair_max - data.Tmax_sim.mean(dim="sample")) / data.Tmax_sim.std(dim="sample")
    pres_all = xr.Dataset({
        'prec_pres': pres_prec,
        'Tavg_pres': pres_Tavg,
        'Tmin_pres': pres_Tmin,
        'Tmax_pres': pres_Tmax,
    })
    # quantiles
    qnames = {k: f"{k}_quantiles" for k in data.data_vars.keys()}
    data_quantiles = data_train.quantile(quantiles, dim="time").rename(qnames)
    return xr.merge([monthly_all, pres_all, data_quantiles])

# %%
results_dict = dict()
for period in tqdm(periods):
    filepath = os.path.join(results_dir, f"{args['model']}_svi_eobs_basin_{basin_info.id}_{period[0]}-{period[1]}.pkl")
    with open(filepath, "rb") as f:
        fit_data = pickle.load(f)
    valid_basin_data = basin_data[["Tair_mean","Tair_min","Tair_max","prec"]].loc[timestamps].copy()
    obs = valid_basin_data.to_xarray().astype(np.float32).rename(index="time")
    prec_occ_obs = (obs["prec"] > 0).astype(np.float32).rename("prec_occ")
    prec_amt_obs = obs["prec"].where(obs["prec"] > 0, np.nan).rename("prec_amt")
    Tavg_pred = xr.DataArray(fit_data["preds"]["Tavg"].squeeze(), dims=("sample","time"), coords={"time": timestamps.values}, name="Tavg_sim")
    Tmin_pred = xr.DataArray(fit_data["preds"]["Tmin"].squeeze(), dims=("sample","time"), coords={"time": timestamps.values}, name="Tmin_sim")
    Tmax_pred = xr.DataArray(fit_data["preds"]["Tmax"].squeeze(), dims=("sample","time"), coords={"time": timestamps.values}, name="Tmax_sim")
    prec_pred = xr.DataArray(fit_data["preds"]["prec"].squeeze(), dims=("sample","time"), coords={"time": timestamps.values}, name="prec_sim")
    prec_occ_pred = (prec_pred > 0).astype(np.float32).rename("prec_occ_sim")
    prec_amt_pred = prec_pred.where(prec_pred > 0, np.nan).rename("prec_amt_sim")
    attrs = {'basin': basin_info.id, 'period': period}
    data = xr.merge([obs, prec_occ_obs, prec_amt_obs, Tavg_pred, Tmin_pred, Tmax_pred, prec_pred, prec_occ_pred, prec_amt_pred]).assign_attrs(**attrs)
    data.to_netcdf(os.path.join(args["outdir"], f"{args['model']}_simulations_basin={basin_info.id}_period={period[0]}-{period[1]}.nc"))
    diagnostics = compute_diagnostics(data, period)
    diagnostics.to_netcdf(os.path.join(args["outdir"], f"{args['model']}_diagnostics_basin={basin_info.id}_period={period[0]}-{period[1]}.nc"))
    results_dict[tuple(period)] = data

print("done")

# %%
