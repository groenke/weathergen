#!/bin/sh

# model glm v2
target1=/data/compoundx/causal_flood/basins_averaged/simulated/wgen_glm_v2/
mkdir -p $target1
cp /work/groenke/wgen-glm-eobs/analysis/wgen_glm_v2*simulations* $target1
# model glm v3
target2=/data/compoundx/causal_flood/basins_averaged/simulated/wgen_glm_v3/
mkdir -p $target2
cp /work/groenke/wgen-glm-eobs/analysis/wgen_glm_v3*simulations* $target2
