# %%
import argparse
import sys
import os
sys.path.append("src/python")

# prevent JAX from preallocating all GPU memory
os.environ["XLA_PYTHON_CLIENT_PREALLOCATE"] = "false"

# data loading and IO
import pandas as pd
import numpy as np
import pickle

# plotting
import matplotlib.pyplot as plt

# pyro
import numpyro
import numpyro.distributions as dist
from numpyro.infer import Predictive
from numpyro.infer.autoguide import AutoMultivariateNormal
from numpyro.handlers import condition, trace
from numpyro.contrib.control_flow import scan, cond

# jax
import jax
import jax.numpy as jnp
import jax.random as random

if any([d.platform == "gpu" for d in jax.devices()]):
    numpyro.set_platform("gpu")
else:
    numpyro.set_host_device_count(jax.local_device_count("cpu"))

from jax.lib import xla_bridge
print(f"current XLA device: {xla_bridge.get_backend().platform}")

import weathergen as wx

models = {
    'wgen_glm_v2': wx.wgen_glm_v2,
    'wgen_glm_v3': wx.wgen_glm_v3,
}

DATA_DIR = "/data/compoundx/causal_flood/basins_averaged"

# default args for interactive mode
args = dict(
    index=0,
    outdir="/work/groenke/wgen-glm-eobs/calibration",
    model="wgen_glm_v3",
    numiter=50000,
    numyears=40,
    batch_size=365,
    learning_rate=1e-3,
    overwrite=False,
    rng_seed=202408,
)

# %%
parser = argparse.ArgumentParser(prog="calibrate-wgen-eobs", description="WGEN-GLM E-OBS basin calibration")
parser.add_argument("index", default=0, type=int)
parser.add_argument('-o', "--outdir", default="../../data/product")
parser.add_argument('-m', '--model', default="wgen_glm_v2")
parser.add_argument('-n', '--numiter', default=50_000, type=int)
parser.add_argument('-b', '--batch-size', default=365, type=int)
parser.add_argument('-l', "--learning-rate", default=1e-4, type=float)
parser.add_argument('-s', '--rng-seed', default=202408, type=int)
parser.add_argument("--numyears", default=40, type=int)
parser.add_argument("--overwrite", default=False, action="store_true")

args = vars(parser.parse_args())
print(args)

# %%
basins_info = pd.read_csv(os.path.join(DATA_DIR, "basins_info.csv"))
# select all basins in Germany (236 total)
basin_info_de = basins_info.loc[basins_info.country == "DE",:]
basin_info = basin_info_de.iloc[args["index"]]
# define name map for E-OBS basin dataset
name_map = wx.data.data_var_name_map(prec="pre", Tair_mean="tavg", Tair_min="tmin", Tair_max="tmax")
# load basin data
basin_data = wx.data.load_time_series_csv(os.path.join(DATA_DIR, "observational", f"{basin_info.id}.csv"), name_map)
# optional: add long-term (decadal) trend as predictor
basin_data["decade"] = np.linspace(0, len(basin_data.index.year.unique())/10, basin_data.shape[0])
# if Tair_mean violates sanity checks, replace with midpoint
basin_data["Tair_mean"] = basin_data.Tair_mean.where(
    np.logical_and(basin_data.Tair_mean > basin_data.Tair_min, basin_data.Tair_mean < basin_data.Tair_max),
    (basin_data.Tair_min + basin_data.Tair_max) / 2
)
# set any remaining rows which violate the consistency check to nan
basin_data = basin_data.where(
    np.logical_and(basin_data.Tair_mean > basin_data.Tair_min, basin_data.Tair_mean < basin_data.Tair_max),
    np.nan,                 
)

print(f"loaded data for basin {basin_info.id} with {basin_data.shape[0]} samples")
print(basin_info)
# assert np.isfinite(basin_data[["Tair_mean","Tair_min","Tair_max","prec"]]).all().all(), "found nan/inf values in dataset"
assert (basin_data.dropna().Tair_max > basin_data.dropna().Tair_min).all(), "found invalid data values; Tair_max <= Tair_min"
assert (basin_data.dropna().Tair_max > basin_data.dropna().Tair_mean).all(), "found invalid data values; Tair_max <= Tair_mean"
assert (basin_data.dropna().Tair_mean > basin_data.dropna().Tair_min).all(), "found invalid data values; Tair_mean <= Tair_min"

# %%
# get time periods for calibration
t_start = basin_data.index.year[0]
t_stop = t_start + args["numyears"]
period = (str(t_start), str(t_stop))
basin_data_train = basin_data[period[0]:period[1]]
Tavg_obs_fft = wx.utils.truncated_fft(basin_data_train.Tair_mean.values, nfreqs=4)
Tavg_obs_fft["period"] = 1/Tavg_obs_fft.freq
print(Tavg_obs_fft)
prec_obs_fft = wx.utils.truncated_fft(basin_data_train.prec.values, nfreqs=4)
prec_obs_fft["period"] = 1/prec_obs_fft.freq
print(prec_obs_fft)

# %%
model = models[args["model"]]
output_dir = os.path.join(args["outdir"], args['model'])
os.makedirs(output_dir, exist_ok=True)
filepath = os.path.join(output_dir, f"{args['model']}_svi_eobs_basin_{basin_info.id}_{period[0]}-{period[1]}.pkl")
# check if file already exists
if os.path.exists(filepath) and not args["overwrite"]:
    print(f"output file already exists at {filepath}; aborting")
    sys.exit(0)

# %%
print(f"fitting model for period {period}")
Tair_freqs = Tavg_obs_fft.freq
prec_freqs = prec_obs_fft.freq
predictors = [] #["decade"]
pred_effect_scale = jnp.ones(1)
wgen_train = wx.WGEN(basin_data_train, model, order=2, pred_effect_scale=pred_effect_scale, Tavg_dof_mean=20.0,
                     Tair_freqs=Tair_freqs, prec_freqs=prec_freqs, predictors=predictors)
guide = AutoMultivariateNormal(wgen_train.step, init_loc_fn=numpyro.infer.init_to_median, init_scale=0.1)
optim = numpyro.optim.Adam(1e-4)
prng = jax.random.PRNGKey(args["rng_seed"])
svi_result, _ = wgen_train.fit("svi", args["numiter"], guide, subsample_time=args["batch_size"], optimizer=optim, rng=prng)
svi_posterior = guide.get_posterior(svi_result.params)

print("running simulations")
wgen_test = wx.WGEN(basin_data, model, order=2, pred_effect_scale=pred_effect_scale, Tavg_dof_mean=20.0,
                    Tair_freqs=Tair_freqs, prec_freqs=prec_freqs, predictors=predictors)
svi_predictive = Predictive(wgen_test.simulate, guide=guide, params=svi_result.params, num_samples=200, parallel=True)
svi_preds = svi_predictive(prng)
with open(filepath, "wb+") as f:
    savedata = dict(result=svi_result, posterior=svi_posterior, preds=svi_preds, args=args)
    pickle.dump(savedata, f, protocol=pickle.HIGHEST_PROTOCOL)
    print(f'saved output to {filepath}')

# %%
